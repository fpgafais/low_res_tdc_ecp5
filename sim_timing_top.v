`timescale 1ps/1fs
//`default nettype none


module testbench;

  reg [7:0]clks;
	reg [0:0]trigger;
	wire valid_led;
	wire locked;
	wire [15:0]tdc_raw;
  wire [7:0]pll_clks;
  wire [31:0]fifo_out;
  reg [0:0]fifo_read;
  wire [0:0]fifo_empty;
  reg frame_sync;

	integer f;
	initial begin
	  #4000000 $stop;
	end
	always @(posedge trigger)begin
	  //$fwrite(f,"trig  %t\n",$time);
	end
	always @(posedge valid_led)begin
	  //$fwrite(f,"data %t %d\n",$time,data_leds);
	end
	always @(posedge clks[0])begin
	  //$fwrite(f,"clk0r %t\n",$time);
	end
	generate
	  genvar i;
	  for(i=0;i<2;i=i+1)begin
	     always @(posedge clks[0])begin
	       if(~fifo_empty[i])fifo_read[i] <= 1;
	       else fifo_read[i] <= 0;
	     end
	  end
	endgenerate
generate
    for(i=0;i<8;i=i+1)begin
      initial fork: sti
        #(250*i) forever #2000 clks[i] = !clks[i];
        join
    end

endgenerate


initial fork: stimulus
    clks = 'b0;
    trigger = 0;
    frame_sync = 0;
    forever #19999 trigger[0] = ~trigger[0];
    forever #200000 frame_sync = ~frame_sync;
  join


top16multich top_inst(
        .clk(clks[0]),
        .marker(frame_sync),
        .trigger(trigger),
        .fifo_out(fifo_out),
        .fifo_empty(fifo_empty),
        .fifo_read(fifo_read)
        );

endmodule
//`default nettype wire
