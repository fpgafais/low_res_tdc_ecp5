module tdc16(in,clks,out);
	input wire in;
	input wire [15:0]clks;
	output wire [15:0]out;
	
	reg [15:0]in_clk_synced;
	//reg [1:0]in_dl[15:0];
		generate
			genvar i;
			for(i=0;i<16;i=i+1)begin
				reg [1:0]in_dl;
				//reg in_clk_synced;
				assign in_clk_synced_rising= ~in_clk_synced[i] & in_dl[1];
				assign out[i] = in_clk_synced_rising;
				always @(posedge clks[i])begin
					{in_clk_synced[i],in_dl} <= {in_dl, in};
				end
				/*always @(posedge clks[i])begin
					if(in_clk_synced_rising)begin
						out[i] <= 1'b1;
					end else begin
						out[i] <= 1'b0;
					end
				end*/
			end
		endgenerate
	endmodule