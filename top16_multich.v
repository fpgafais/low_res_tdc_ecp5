module top16multich(clk,marker,trigger,fifo_out,fifo_empty,fifo_read);
	parameter CHANNELS = 1;
	parameter CNT_WIDTH = 28;
	parameter TDC_LATENCY = 2;

	input wire clk;
	input wire marker;
	input wire [CHANNELS-1:0]trigger;
	output wire [$clog2(CHANNELS):0][31:0]fifo_out;
	output wire [CHANNELS-1:0]fifo_empty;
	input wire [CHANNELS-1:0]fifo_read;
	//output wire[15:0]tdc_raw;
	//output wire[7:0]tdc_raw_up;
	//output wire[7:0]tdc_raw_down;
	//output wire[15:0]synced_raw;output wire data_valid;
	wire [$clog2(CHANNELS):0][3:0]decoder_out;
	wire [CHANNELS-1:0]decoder_data_valid;
	reg [$clog2(CHANNELS):0][CNT_WIDTH-1+4:0]data;
	wire [7:0]pll_clks;
	wire [1:0]pll_locked;
	
	wire [CNT_WIDTH-1:0]cnt;
	
	reg [1:0]marker_dl;
	reg [TDC_LATENCY:0][27:0]cnt_dl;

	assign cnt = cnt_dl[2];
	assign marker_rising = ~marker_dl[1] & marker_dl[0];

	always @(posedge clk)begin
			marker_dl[1]<=marker_dl[0];
			marker_dl[0]<=marker;
	end
	genvar i;	
	generate
		for(i=0;i<TDC_LATENCY;i=i+1)begin
			always @(posedge clk)begin
				cnt_dl[i+1]<=cnt_dl[i];
			end
		end
	endgenerate
	
	always @(posedge clk)begin
		if(marker_rising)cnt_dl[0]<=0;
			else cnt_dl[0]<=cnt_dl[0]+28'b1;
		end

	pll0 pll0inst(.CLKI( clk), .CLKOP( pll_clks[0]), .CLKOS(pll_clks[1]), .CLKOS2(pll_clks[2]), .CLKOS3(pll_clks[3]), .LOCK(pll_locked[0]));
	pll1 pll1inst(.CLKI( clk), .CLKOP( pll_clks[4]), .CLKOS(pll_clks[5]), .CLKOS2(pll_clks[6]), .CLKOS3(pll_clks[7]), .CLKFB( pll_clks[0]), .LOCK(pll_locked[1]));
	
	wire [15:0]tdc_out[CHANNELS-1:0];
	
generate
	for(i=0;i<CHANNELS;i=i+1)begin
		always @(posedge clk) data[i]<={cnt,decoder_out[i]};
		tdc8ddr tdc8ddr_inst(
									.trigger(trigger[i]), 
									.clks(pll_clks), 
									.out(tdc_out[i]) 
									);
		output_decoder16 output_decoder16_inst(
																.clk(pll_clks[7]), 
																.in(tdc_out[i]), 
																.out(decoder_out[i]), 
																.valid(decoder_data_valid[i])
																);
		fifo0 fifo0_inst (
							.Data( data[i]), 
							.Clock( pll_clks[0]), 
							.WrEn( decoder_data_valid[i]), 
							.RdEn( fifo_read[i]), 
							.Reset( ), 
							.Q( fifo_out[i]), 
							.Empty( fifo_empty[i]), 
							.Full( ), 
							.AlmostEmpty( ), 
							.AlmostFull( )
							);
	end
endgenerate

endmodule