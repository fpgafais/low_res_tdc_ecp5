module tdc8ddr(trigger, clks, out);
	input wire trigger;
	input wire[7:0]clks;
	output wire [15:0]out;
	
	reg [7:0]in_clk_synced;
	reg [7:0]in_clk_down_synced;
	generate
		genvar i;
		for(i=0;i<8;i=i+1)begin
			reg [1:0]in_up_dl;
			reg [1:0]in_down_dl;
			assign out[i]=~in_clk_synced[i] & in_up_dl[1];
			assign out[8+i]= ~in_clk_down_synced[i] & in_down_dl[1];
			//assign out_up[i]=~in_clk_synced[i] & in_up_dl[1];
			//assign out_down[i]=~in_clk_down_synced[i] & in_down_dl[1];
			always @(posedge clks[i])begin
				{in_clk_synced[i],in_up_dl} <= {in_up_dl,trigger};
			end
			always @(negedge clks[i])begin
				{in_clk_down_synced[i],in_down_dl} <= {in_down_dl,trigger};
			end
		end
	endgenerate
endmodule