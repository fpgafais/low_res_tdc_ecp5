module output_decoder16(clk,in,out,valid);

input wire clk;
input wire [15:0]in;
output reg [3:0]out;
output reg valid;

reg [15:0]dl[2:0];
reg [15:0]in_synced;

always @ (posedge clk)begin
	in_synced <= dl[0];
	dl[0] <= dl[1];
	dl[1] <= in;
	end
	always @ (negedge clk)begin
		case (in)			
			/*16'b1000000000000000 : out <= 4'b0000;
			16'b1000000000000001 : out <= 4'b0001;
			16'b1000000000000011 : out <= 4'b0010;
			16'b1000000000000111 : out <= 4'b0011;
			16'b1000000000001111 : out <= 4'b0100;
			16'b1000000000011111 : out <= 4'b0101;
			16'b1000000000111111 : out <= 4'b0110;
			16'b1000000001111111 : out <= 4'b0111;
			16'b1000000011111111 : out <= 4'b1000;
			16'b1000000111111111 : out <= 4'b1001;
			16'b1000001111111111 : out <= 4'b1010;
			16'b1000011111111111 : out <= 4'b1011;
			16'b1000111111111111 : out <= 4'b1100;
			16'b1001111111111111 : out <= 4'b1101;
			16'b1011111111111111 : out <= 4'b1110;
			16'b1111111111111111 : out <= 4'b1111;
			*/
			16'b1000000000000000 : begin
                        out <= 4'b0000;
                        valid <= 1'b1;
                        end
16'b1000000000000001 : begin
                        out <= 4'b0001;
                        valid <= 1'b1;
                        end
16'b1000000000000011 : begin
                        out <= 4'b0010;
                        valid <= 1'b1;
                        end
16'b1000000000000111 : begin
                        out <= 4'b0011;
                        valid <= 1'b1;
                        end
16'b1000000000001111 : begin
                        out <= 4'b0100;
                        valid <= 1'b1;
                        end
16'b1000000000011111 : begin
                        out <= 4'b0101;
                        valid <= 1'b1;
                        end
16'b1000000000111111 : begin
                        out <= 4'b0110;
                        valid <= 1'b1;
                        end
16'b1000000001111111 : begin
                        out <= 4'b0111;
                        valid <= 1'b1;
                        end
16'b1000000011111111 : begin
                        out <= 4'b1000;
                        valid <= 1'b1;
                        end
16'b1000000111111111 : begin
                        out <= 4'b1001;
                        valid <= 1'b1;
                        end
16'b1000001111111111 : begin
                        out <= 4'b1010;
                        valid <= 1'b1;
                        end
16'b1000011111111111 : begin
                        out <= 4'b1011;
                        valid <= 1'b1;
                        end
16'b1000111111111111 : begin
                        out <= 4'b1100;
                        valid <= 1'b1;
                        end
16'b1001111111111111 : begin
                        out <= 4'b1101;
                        valid <= 1'b1;
                        end
16'b1011111111111111 : begin
                        out <= 4'b1110;
                        valid <= 1'b1;
                        end
16'b1111111111111111 : begin
                        out <= 4'b1111;
                        valid <= 1'b1;
                        end
			default   :  begin
						out <=4'b1111;
						valid <= 1'b0;
						end
			endcase
		end


endmodule